import Foundation

//массивы кол-во дней и месяца
let dayinmonth: [Int] = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
let month: [String] = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"]
//вывод кол-во дней
for i in dayinmonth{
    
    print(i)
    
}

print("")

var n = 0
//вывод месяц+день
for k in dayinmonth{
    
    print("\(month[n]) - \(k)")
    n += 1
    
}

print("")
//вывод с помощью тюплес
var tuplesCalendar: [(String, Int)] = [("Январь", 31), ("Февраль", 28), ("Март", 31), ("Апрель", 30), ("Май", 31), ("Июнь", 30), ("Июль", 31), ("Август", 31), ("Сентябрь", 30), ("Октябрь",31), ("Ноябрь", 30), ("Декабрь",31)]

for j in 0..<tuplesCalendar.count{
    
    print("\(tuplesCalendar[j].0) - \(tuplesCalendar[j].1)")
    
}

print("")
//в обратном порядке
for g in (0..<tuplesCalendar.count).reversed(){
    
    print("\(tuplesCalendar[g].0) - \(tuplesCalendar[g].1)")
    
}

print("")
//сколько до даты
//начало года
var comp1 = DateComponents()
comp1.year = 2022
comp1.month = 1
comp1.day = 1
let date1 = (Calendar.current).date(from: comp1)
//произвольная дата
var comp2 = DateComponents()
comp2.year = 2022
comp2.month = 1
comp2.day = 20
let date2 = (Calendar.current).date(from: comp2)

let howdays = (date2?.timeIntervalSince(date1! ))!/86400
print(howdays)
